# -*- encoding: utf-8 -*-
require File.expand_path('../lib/dotard/version', __FILE__)

Gem::Specification.new do |gem|
  gem.name          = "dotard"
  gem.version       = Dotard::VERSION
  gem.authors       = ["Ryan Leto"]
  gem.email         = ["ryjleto@gmail.com"]
  gem.description   = %q{A Heroku-style random name generator}
  gem.summary       = %q{A Heroku-style random name generator}
  gem.homepage      = "https://github.com/ryleto/dotard"
  gem.license       = "MIT"
  gem.files         = `git ls-files`.split("\n")
  gem.executables   = []
  gem.test_files    = `git ls-files -- test/*`.split("\n")
  gem.require_paths = ["lib"]
end
