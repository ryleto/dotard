require "dotard/version"

module Dotard
  Dotard::Error = Class.new(StandardError)

  def self.adjective(options = {})
    get_item("adjectives", options)
  end

  def self.noun(options = {})
    get_item("nouns", options)
  end
  
  def self.abstract(options = {})
    get_item("abstracts", options)
  end

  def self.phrase(options = {})
    options[:max_length] /= 2 if options[:max_length]
    options[:min_length] /= 2 if options[:min_length]
    adjective(options) + noun(options)
  end

  def self.get_item(filename, options = {})
    items = items_from_file(filename)
    items.select!{ |item| item.length <= options[:max_length] } if options[:max_length]
    items.select!{ |item| item.length >= options[:min_length] } if options[:min_length]
    items.sample || fail(Dotard::Error, "No words found")
  end

  def self.items_from_file(filename)
    filepath = File.expand_path("../dotard/#{filename}.txt", __FILE__)
    File.read(filepath).split("\n")
  end
end
