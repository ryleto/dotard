require "minitest/autorun"
require "dotard"

class TestDotard < Minitest::Test
  def all_adjectives
    @all_adjectives ||= Dotard.items_from_file("adjectives")
  end

  def all_nouns
    @all_nouns ||= Dotard.items_from_file("nouns")
  end
  
  def all_abstracts
    @all_abstracts ||= Dotard.items_from_file("abstracts")
  end
  
  def test_adjective
    adjective = Dotard.adjective
    refute_empty adjective
    assert_includes all_adjectives, adjective
  end

  def test_adjective_max_length
    adjective = Dotard.adjective(:max_length => 4)
    assert adjective.length <= 4
  end

  def test_adjective_invalid_max_length
    assert_raises Dotard::Error do
      Dotard.adjective(:max_length => 1)
    end
  end

  def test_adjective_min_length
    adjective = Dotard.adjective(:min_length => 8)
    assert adjective.length >= 8
  end

  def test_adjective_invalid_min_length
    assert_raises Dotard::Error do
      Dotard.adjective(:min_length => 100)
    end
  end

  def test_noun
    noun = Dotard.noun
    refute_empty noun
    assert_includes all_nouns, noun
  end

  def test_noun_max_length
    noun = Dotard.noun(:max_length => 4)
    assert noun.length <= 4
  end

  def test_noun_invalid_max_length
    assert_raises Dotard::Error do
      Dotard.noun(:max_length => 1)
    end
  end

  def test_noun_min_length
    noun = Dotard.noun(:min_length => 8)
    assert noun.length >= 8
  end

  def test_noun_invalid_min_length
    assert_raises Dotard::Error do
      Dotard.noun(:min_length => 100)
    end
  end
  
  def test_abstract
    abstract = Dotard.abstract
    refute_empty abstract
    assert_includes all_abstracts, abstract
  end

  def test_abstract_max_length
    abstract = Dotard.abstract(:max_length => 4)
    assert abstract.length <= 4
  end

  def test_abstract_invalid_max_length
    assert_raises Dotard::Error do
      Dotard.abstract(:max_length => 1)
    end
  end

  def test_abstract_min_length
    abstract = Dotard.abstract(:min_length => 8)
    assert abstract.length >= 8
  end

  def test_abstract_invalid_min_length
    assert_raises Dotard::Error do
      Dotard.abstract(:min_length => 100)
    end
  end

  def test_username
    phrase = Dotard.phrase
    refute_empty phrase
    anchor = (2..phrase.length).detect do |i|
      all_adjectives.include?(phrase[0..i])
    end
    assert_includes all_adjectives, phrase[0..anchor]
    assert_includes all_nouns, phrase[anchor+1..-1]
  end

  def test_username_max_length
    phrase = Dotard.phrase(:max_length => 10)
    assert phrase.length <= 10
  end

  def test_username_invalid_max_length
    assert_raises Dotard::Error do
      Dotard.phrase(:max_length => 2)
    end
  end

  def test_username_min_length
    phrase = Dotard.phrase(:min_length => 10)
    assert phrase.length >= 10
  end

  def test_username_invalid_min_length
    assert_raises Dotard::Error do
      Dotard.phrase(:min_length => 100)
    end
  end
end
